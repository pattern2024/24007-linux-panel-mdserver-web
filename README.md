# 星火Linux面板(内部开发使用)

### 安装

```
curl https://gitlab.com/pattern2024/24007-linux-panel-mdserver-web/-/raw/pattern/scripts/install_dev.sh?ref_type=heads | sudo bash



curl https://gitlab.com/pattern2024/24007-linux-panel-mdserver-web/-/raw/pattern/scripts/install_pve8.sh | bash
```

### 源码阅读

#### 基础知识

1. flask https://tutorial.helloflask.com/
1. shell
1. html css js jquery bootstrap layui

#### 源码阅读分析

1. 执行流程

### 简介

简单的Linux面板,感谢BT.CN写出如此好的web管理软件。我一看到，就知道这是我一直想要的页面化管理方式。
复制了后台管理界面，按照自己想要的方式写了一版。


* SSH终端工具
* 面板收藏功能
* 网站子目录绑定
* 网站备份功能
* 插件方式管理

基本上可以使用,后续会继续优化!欢迎提供意见！

```
如果出现问题，最好私给我面板信息。不要让我猜。如果不提供，不要提出问题，自行解决。  — 座右铭
Talk is cheap, show me the code.  -- linus
```

- [兼容性测试报告](/compatibility.md)
- [常用命令说明](/cmd.md) [ mw default ] [ mw update_dev ]

### 主要插件介绍

* OpenResty - 轻量级，占有内存少，并发能力强。
* PHP[53-83] - PHP是世界上最好的编程语言。
* MySQL - 一种关系数据库管理系统。
* MariaDB - 是MySQL的一个重要分支。
* MySQL[APT/YUM] - 一种关系数据库管理系统。
* MongoDB - 一种非关系NOSQL数据库管理系统。
* phpMyAdmin - 著名Web端MySQL管理工具。
* Memcached - 一个高性能的分布式内存对象缓存系统。
* Redis - 一个高性能的KV数据库。
* PureFtpd - 一款专注于程序健壮和软件安全的免费FTP服务器软件。
* Gogs - 一款极易搭建的自助Git服务。
* Rsyncd - 通用同步服务。


# Note

```
phpMyAdmin[4.4.15]支持MySQL[5.5-5.7]
phpMyAdmin[5.2.0]支持MySQL[8.0]

PHP[53-72]支持phpMyAdmin[4.4.15]
PHP[72-83]支持phpMyAdmin[5.2.0]
```


### 版本更新 0.17.0

- 面板SSL调整。

