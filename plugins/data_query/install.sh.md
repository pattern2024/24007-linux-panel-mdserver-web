**总结**：

这段 Bash 脚本的主要功能是安装或卸载一个应用程序或组件。根据传入的第一个参数（`$1`），脚本会执行相应的安装或卸载操作。安装时，它会创建一些目录，并写入版本号；卸载时，它会删除特定的目录。

**逐行解释**：

1. `#!/bin/bash`
   - 指定脚本使用 Bash 解释器来执行。

2-3. 设置并导出 PATH 环境变量，加入 `/opt/homebrew/bin` 目录，这通常是 macOS 上 Homebrew 工具的二进制文件路径。

5. `curPath=`pwd``
   - 获取当前工作目录的路径，并赋值给 `curPath` 变量。

6. `rootPath=$(dirname "$curPath")`
   - 获取 `curPath` 的父目录路径，并赋值给 `rootPath` 变量。

7. `rootPath=$(dirname "$rootPath")`
   - 再次获取 `rootPath` 的父目录路径，这实际上是获取当前工作目录的上级的上级目录。

8. `serverPath=$(dirname "$rootPath")`
   - 获取 `rootPath` 的父目录路径，此时 `serverPath` 是当前工作目录的上级的上级的上级目录。

10. `install_tmp=${rootPath}/tmp/mw_install.pl`
   - 定义一个临时安装文件的路径。

11. `VERSION=$2`
   - 将脚本的第二个参数（`$2`）赋值给 `VERSION` 变量，这个变量通常代表应用程序或组件的版本号。

13-19. `Install_App` 函数定义：
   - `echo '正在安装脚本文件...' > $install_tmp`：在临时安装文件中写入 "正在安装脚本文件..."。
   - `mkdir -p $serverPath/source`：在 `serverPath` 下创建名为 `source` 的目录。
   - `mkdir -p $serverPath/data_query`：在 `serverPath` 下创建名为 `data_query` 的目录。
   - `echo "${VERSION}" > $serverPath/data_query/version.pl`：将版本号写入 `data_query` 目录下的 `version.pl` 文件。
   - `echo '安装完成'`：在终端输出 "安装完成"。

21-24. `Uninstall_App` 函数定义：
   - `rm -rf $serverPath/data_query`：删除 `serverPath` 下的 `data_query` 目录及其内容。
   - `echo "卸载成功"`：在终端输出 "卸载成功"。

26. `action=$1`
   - 将脚本的第一个参数赋值给 `action` 变量。

28-32. 判断 `action` 的值：
   - 如果 `action` 为 'install'，则执行 `Install_App` 函数进行安装。
   - 如果不是（默认情况），则执行 `Uninstall_App` 函数进行卸载。

注意：

- 脚本中的 `version.pl` 文件扩展名可能是一个笔误。通常，对于 Python 环境，我们会使用 `.py` 或 `.txt` 作为文件扩展名，而不是 `.pl`（这通常是 Perl 脚本的扩展名）。如果确实要使用 `.pl`，请确保这是有意为之。
- `install_tmp` 文件用于记录安装过程中的状态信息，但在当前的 `Install_App` 函数中并没有进一步使用这个文件，只是写入了初始的安装提示。如果后续有进一步的操作或需要记录更多的信息，可以考虑使用这个文件。
- 脚本只创建了 `source` 和 `data_query` 这两个目录，并没有进行实际的文件复制、权限设置或其他安装步骤。如果这只是一个示例或框架，那么后续可能还需要添加更多的安装逻辑。