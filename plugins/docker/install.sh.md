**总结**：

这段 Bash 脚本的主要功能是安装或卸载 Docker 及其相关组件。根据传入的第一个参数（`$1`），脚本会执行相应的安装或卸载操作。安装时，它会检查 Docker 是否已安装，如果没有，则使用 `curl` 命令下载并安装 Docker，然后安装 Python 的 `docker` 和 `pytz` 库，并创建 Docker 的版本文件；卸载时，它会停止并禁用 Docker 服务，然后删除 Docker 及其相关包，并删除 Docker 目录。

**逐行解释**：

1-3. 设置 PATH 环境变量，并导出，使得脚本可以在这些路径下找到命令。

5. 获取当前工作目录的路径，并赋值给 `curPath` 变量。

6-7. 获取 `curPath` 的父目录，并再次获取其父目录，分别赋值给 `rootPath`。

8. 获取 `rootPath` 的父目录，赋值给 `serverPath`。

11-12. 这两行是被注释掉的命令，它们表示了如何调用这个脚本进行安装或卸载操作。

14. 定义 `install_tmp` 变量，这是临时安装文件的路径。

15. 获取脚本的第二个参数，赋值给 `VERSION` 变量，通常用于表示版本信息。

17-19. 检查是否存在 `${rootPath}/bin/activate` 文件（这通常是虚拟环境的激活脚本），如果存在则激活它。

21-44. `Install_Docker` 函数定义：

- `echo '正在安装脚本文件...' > $install_tmp`：在临时安装文件中写入 "正在安装脚本文件..."。
- `mkdir -p $serverPath/source`：在 `serverPath` 下创建名为 `source` 的目录。
- 检查 `serverPath/docker` 目录是否存在，如果不存在，则通过 `curl` 命令下载并安装 Docker。
- 使用 `pip` 安装 `docker` 和 `pytz` Python 库。
- 如果 `serverPath/docker` 目录存在，则写入版本号到 `version.pl` 文件，并标记安装完成。
- 使用 `python3` 运行 `index.py` 脚本的 `start` 和 `initd_install` 方法。

46-66. `Uninstall_Docker` 函数定义：

- 初始化 `CMD` 变量为 `yum`（一个包管理工具）。
- 使用 `which apt` 检查 `apt` 命令是否存在，如果存在，则将 `CMD` 设置为 `apt`。
- 检查是否存在 Docker 的 systemd 服务文件，如果存在，则停止并禁用 Docker 服务，删除服务文件，并重新加载 systemd 守护进程。
- 使用 `CMD` 命令移除 Docker 及其相关组件。
- 删除 `serverPath/docker` 目录。
- 在临时安装文件中写入 "Uninstall_Docker"。

68. 获取脚本的第一个参数，赋值给 `action` 变量。

70-73. 判断 `action` 的值，如果是 'install'，则调用 `Install_Docker` 函数进行安装；否则，调用 `Uninstall_Docker` 函数进行卸载。

注意：

- 脚本中的 `version.pl` 文件扩展名可能是一个错误，因为 Docker 的版本文件通常使用 `.txt` 或无扩展名。
- 脚本在 `Install_Docker` 函数中尝试使用 `pip` 安装 Python 库，但没有指定 Python 版本，这可能会导致使用系统默认的 Python 版本，而不是虚拟环境中的版本（如果已激活）。
- 脚本在卸载 Docker 时仅删除了几个特定的包，但可能还有其他相关包需要被删除。
- 脚本在 `index.py` 脚本中运行 `start` 和 `initd_install` 方法，但没有提供这些方法的具体实现和它们的作用。这些可能是特定于应用程序的逻辑。