#!/usr/bin/env bash

function redis_build()
{
    if [ ! -f redis-6.2.7.tar.gz ];then
        wget -O redis-6.2.7.tar.gz http://download.redis.io/releases/redis-6.2.7.tar.gz
    fi
    
    if [ !-d redis-6.2.7 ];then
        tar -zxvf redis-6.2.7.tar.gz
    fi
    cd redis-6.2.7
    ls -al
    gmake PREFIX=/opt/ros_redis install
}

function redis_config()
{
    echo "1"
}


function redis_service()
{
    echo "2"
}

redis_build

