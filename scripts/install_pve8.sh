#!/usr/bin/env bash
set -x

echo "" > /etc/apt/sources.list

rm -rf /etc/apt/sources.list.d/
mkdir -p /etc/apt/sources.list.d/

curl https://gitlab.com/pattern2024/24007-linux-panel-mdserver-web-mdserver-web/raw/branch/pattern/scripts/pve/sources.list.d/bookworm.list > /etc/apt/sources.list.d/bookworm.list
curl https://gitlab.com/pattern2024/24007-linux-panel-mdserver-web-mdserver-web/raw/branch/pattern/scripts/pve/sources.list.d/bookworm-updates.list > /etc/apt/sources.list.d/bookworm-updates.list
curl https://gitlab.com/pattern2024/24007-linux-panel-mdserver-web-mdserver-web/raw/branch/pattern/scripts/pve/sources.list.d/pve-no-subscription.list > /etc/apt/sources.list.d/pve-no-subscription.list

apt update -y

apt upgrade -y
mkdir -p /etc/docker/
mkdir ~/.docker/
curl https://gitlab.com/pattern2024/24007-linux-panel-mdserver-web-mdserver-web/raw/branch/pattern/scripts/daemon.json > /etc/docker/daemon.json

apt-get install \
    debootstrap \
    squashfs-tools \
    xorriso \
    isolinux \
    syslinux-efi \
    grub-pc-bin \
    grub-efi-amd64-bin \
    grub-efi-ia32-bin \
    mtools \
    dosfstools -y

apt install default-jdk git expect -y
apt install sudo squashfs-tools-ng -y

    apt install rsync -y
    apt install docker.io -y 
    apt install docker-compose -y 
    systemctl enable docker
    systemctl start docker
    docker version
